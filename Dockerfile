# Base
FROM python:3.9.7-slim-buster AS base
WORKDIR /app
EXPOSE 5000
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY . .
ENV FLASK_APP=src/app.py

FROM base as PRODUCTION
ENV ENV=PRODUCTION
CMD ["python3", "-m", "gunicorn", "--preload", "--chdir", "src/", "-b", "0.0.0.0:5000", "app:app"]

FROM base as DEVELOPMENT
ENV ENV=DEVELOPMENT
CMD ["python3", "-m", "gunicorn", "--reload", "--chdir", "src/", "-b", "0.0.0.0:5000", "app:app"]
