import os, sys
from flask import Flask

try:
    print(os.environ["ENV"])
except Exception as e:
    print("Missing environment variable :" + str(e))
    sys.exit(1)

app = Flask(__name__)

import route.login
