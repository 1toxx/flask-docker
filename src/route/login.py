from app import *
from flask import Blueprint, request
from werkzeug.exceptions import BadRequest
from utils import *

login = Blueprint("login", __name__)

# Login to an application
@login.route("/<application>", methods=["POST"])
@is_graphql
def home(input, application):
    print(input)

    try:
        email = input["email"]
        password = input["password"]

        return {
            "email": email,
            "password": password
        }
    except KeyError as field:
        # FIXME: implement app error middleware to answer JSON
        raise BadRequest(description = f"field {field} required")

app.register_blueprint(login, url_prefix="/login")
