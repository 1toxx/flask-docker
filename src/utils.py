from functools import wraps
from flask import request
from werkzeug.exceptions import BadRequest

# Ensure the request contains required fields
def is_graphql(endpoint):
    @wraps(endpoint)
    def wrapper(*args, **kwargs):
        input = request.json.get("input")

        if input == None:
            raise BadRequest(description = "graphql input required")

        return endpoint(input, *args, **kwargs)

    return wrapper
